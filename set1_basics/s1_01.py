# converting hex to base64
import binascii


def hex2b64(hexstr):
    hexbytes = binascii.a2b_hex(hexstr)
    encoded = binascii.b2a_base64(hexbytes)

    return encoded


print(hex2b64('49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d'))
