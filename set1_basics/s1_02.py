# xor two hex strings
import binascii


def hex_xor(s1, s2):
    s1, s2 = binascii.a2b_hex(s1), binascii.a2b_hex(s2)
    xorbytes = binascii.b2a_hex(bytes(x ^ y for x, y in zip(s1, s2)))

    return xorbytes


print(hex_xor('1c0111001f010100061a024b53535009181c', '686974207468652062756c6c277320657965'))
